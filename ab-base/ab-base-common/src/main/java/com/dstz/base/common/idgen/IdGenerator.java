package com.dstz.base.common.idgen;

/**
 * ID生成器
 *
 * @author wacxhs
 */
public interface IdGenerator {

    /**
     * 唯一ID
     *
     * @return 唯一ID
     */
    String nextId();
}
