package com.dstz.base.common.constats;

/**
 * MDC 常量定义
 *
 * @author wacxhs
 */
public class MDCConstant {

	/**
	 * 请求追踪 ID
	 */
	public static String TRACE_ID = "traceId";

	/**
	 * 租户ID
	 */
	public static String TENANT_ID = "tenantId";

	/**
	 * 租户别名
	 */
	public static String TENANT_ALIAS = "tenantAlias";
}
