package com.dstz.component.mq.msg.engine.core.mapper;

import com.dstz.base.mapper.AbBaseMapper;
import com.dstz.component.mq.msg.engine.core.entity.MessageTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lightning
 * @since 2022-11-14
 */
@Mapper
public interface MessageTemplateMapper extends AbBaseMapper<MessageTemplate> {

}
