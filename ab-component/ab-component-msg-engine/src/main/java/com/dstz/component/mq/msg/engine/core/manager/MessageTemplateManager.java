package com.dstz.component.mq.msg.engine.core.manager;

import com.dstz.base.manager.AbBaseManager;
import com.dstz.component.mq.msg.engine.core.entity.MessageTemplate;

/**
 * <p>
 * 通用业务类
 * </p>
 *
 * @author lightning
 * @since 2022-11-14
 */
public interface MessageTemplateManager extends AbBaseManager<MessageTemplate> {


}
