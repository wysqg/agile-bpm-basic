package com.dstz.component.j2cache.constant;

/**
 * j2cache 二级缓存属性键常量
 *
 * @author wacxhs
 */
public class J2CacheBroadcastPropertyKeyConstant {

    /**
     * 广播是否开启
     */
    public static final String OPEN = "open";

    /**
     * 通道名称
     */
    public static final String CHANNEL = "channel";

    /**
     * redis listener container bean id
     */
    public static final String REDIS_MESSAGE_LISTENER_CONTAINER_BEAN_ID = "redisMessageListenerContainerBeanId";
}
