package com.dstz.groovy.script.api;

/**
 * 脚本接口。 <br />
 * <p>
 * 实现了该接口的spring bean 将会被注入到脚本执行引擎中
 *
 * @author jeff
 * @since 2022-01-26
 */
public interface IScript {
}
