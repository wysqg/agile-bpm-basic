package com.dstz.demo.core.mapper;

import com.dstz.demo.core.entity.BizApplyOrder;
import com.dstz.base.mapper.AbBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单信息 Mapper 接口
 * </p>
 *
 * @author wacxhs
 * @since 2022-01-20
 */
@Mapper
public interface BizApplyOrderMapper extends AbBaseMapper<BizApplyOrder> {

}
