package com.dstz.demo.core.manager.impl;

import com.dstz.base.manager.impl.AbBaseManagerImpl;
import com.dstz.demo.core.entity.BizApplyOrder;
import com.dstz.demo.core.manager.BizApplyOrderManager;
import org.springframework.stereotype.Service;

/**
 * 订单信息 通用服务实现类
 *
 * @author wacxhs
 * @since 2022-01-20
 */
@Service("bizApplyOrderManager")
public class BizApplyOrderManagerImpl extends AbBaseManagerImpl<BizApplyOrder> implements BizApplyOrderManager {

}
