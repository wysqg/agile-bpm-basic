package com.dstz.demo.api;
/**
 * 实现对外部模块的接口，  implements DemoApi
 * 服务接口类似Controller，不能存在事物，仅做数据转换，校验等。逻辑需要下沉到 Mananger 层
 * @author Jeff
 */
public class DemoApiImpl {

}
