package com.dstz.sys.core.mapper;

import com.dstz.base.mapper.AbBaseMapper;
import com.dstz.sys.core.entity.SysFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统附件 Mapper 接口
 * </p>
 *
 * @author jinxia.hou
 * @since 2022-02-17
 */
@Mapper
public interface SysFileMapper extends AbBaseMapper<SysFile> {

}
