SET FOREIGN_KEY_CHECKS=0;

DROP TABLE `a5_gitee_new`.`ab_message_log`;

DROP TABLE `a5_gitee_new`.`appstore_user_install_log`;

DROP TABLE `a5_gitee_new`.`bpm_open_flow`;

 

DROP TABLE `a5_gitee_new`.`bpm_plugin_ensure_candidate`;
 

DROP TABLE `a5_gitee_new`.`bpm_task_duration_statistics`;
  
DROP TABLE `a5_gitee_new`.`cswbsjy2`;

DROP TABLE `a5_gitee_new`.`service_hall`;

DROP TABLE `a5_gitee_new`.`sys_audit_log`;

DROP TABLE `a5_gitee_new`.`sys_audit_log_metadata`;

DROP TABLE `a5_gitee_new`.`sys_crontab`;

DROP TABLE `a5_gitee_new`.`sys_data_privilege`;

DROP TABLE `a5_gitee_new`.`sys_data_privilege_given`;

 

DROP TABLE `a5_gitee_new`.`sys_internation`;

DROP TABLE `a5_gitee_new`.`sys_language`;

 
DROP TABLE `a5_gitee_new`.`sys_serial_no_config`;

DROP TABLE `a5_gitee_new`.`sys_serial_no_revive`;

DROP TABLE `a5_gitee_new`.`sys_serialno`;
  

DROP TABLE `a5_gitee_new`.`sys_work_handover`;

DROP TABLE `a5_gitee_new`.`sys_workbench_layout`;

DROP TABLE `a5_gitee_new`.`sys_workbench_panel`;

DROP TABLE `a5_gitee_new`.`sys_workbench_panel_templ`;

DROP TABLE `a5_gitee_new`.`trx_message_done`;

DROP TABLE `a5_gitee_new`.`trx_message_redo`;

SET FOREIGN_KEY_CHECKS=1;